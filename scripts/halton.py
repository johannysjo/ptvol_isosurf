import numpy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def halton(index, base):
    result = 0.0
    f = 1.0
    i = index
    while i > 0:
        f = f / base;
        result += f * (i % base)
        i = numpy.floor(i / base)
    return result


def halton_uniform_hemisphere(n):
    x = numpy.zeros(n)
    y = numpy.zeros(n)
    z = numpy.zeros(n)
    for i in xrange(0, n):
        zi = halton(i, 2.0)
        phi = 2.0 * numpy.pi * halton(i, 3.0)
        r = numpy.sqrt(1.0 - zi * zi)
        x[i] = r * numpy.cos(phi)
        y[i] = r * numpy.sin(phi)
        z[i] = zi
    return x, y, z


def halton_cosine_hemisphere(n):
    x = numpy.zeros(n)
    y = numpy.zeros(n)
    z = numpy.zeros(n)
    for i in xrange(0, n):
        r = numpy.sqrt(halton(i, 2.0))
        phi = 2.0 * numpy.pi * halton(i, 3.0)
        x[i] = r * numpy.cos(phi)
        y[i] = r * numpy.sin(phi)
        z[i] = numpy.sqrt(1.0 - r * r)
    return x, y, z


def random_uniform_hemisphere(n):
    x = numpy.zeros(n)
    y = numpy.zeros(n)
    z = numpy.zeros(n)
    for i in xrange(0, n):
        zi = numpy.random.rand()
        phi = 2.0 * numpy.pi * numpy.random.rand()
        r = numpy.sqrt(1.0 - zi * zi)
        x[i] = r * numpy.cos(phi)
        y[i] = r * numpy.sin(phi)
        z[i] = zi
    return x, y, z


def random_cosine_hemisphere(n):
    x = numpy.zeros(n)
    y = numpy.zeros(n)
    z = numpy.zeros(n)
    for i in xrange(0, n):
        r = numpy.sqrt(numpy.random.rand())
        phi = 2.0 * numpy.pi * numpy.random.rand()
        x[i] = r * numpy.cos(phi)
        y[i] = r * numpy.sin(phi)
        z[i] = numpy.sqrt(1.0 - r * r)
    return x, y, z


def main():
    n = 200
    x, y, z = halton_uniform_hemisphere(n)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.scatter(x, y, z)
    ax.set_aspect("equal")
    ax.set_xlim3d(-1.0, 1.0)
    ax.set_ylim3d(-1.0, 1.0)
    ax.set_zlim3d(-1.0, 1.0)

    plt.show()


if __name__ == "__main__":
    main()
