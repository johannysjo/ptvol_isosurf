import numpy
import pylab


def rand2f():
    x = numpy.random.rand()
    y = numpy.random.rand()
    return x, y


def hash(index):
    # float seed = dot(vec2(p), vec2(12.9898, 78.2331))
    seed = index * 12.9898
    return fract(sin(seed) * 43758.5453)


def halton(index, base):
    result = 0.0
    f = 1.0
    i = index
    while i > 0:
        f = f / base;
        result += f * (i % base)
        i = numpy.floor(i / base)
    return result


def halton23(index):
    x = halton(index, 2.0)
    y = halton(index, 3.0)
    return x, y


def lds_r2(index):
    phi = 1.324717957244
    x = (index / phi) % 1.0
    y = (index / (phi * phi)) % 1.0
    return x, y


def lds_r2_jittered(index):
    delta = 2.0
    r = delta * 0.76 * numpy.sqrt(numpy.pi) * 0.25 / numpy.sqrt(index - 0.7)
    phi = 1.324717957244
    x_jitter = (numpy.sin(index * 12.9898) * 43758.5453) % 1.0
    y_jitter = (numpy.sin(index * 78.2331) * 43758.5453) % 1.0
    x = ((index / phi) + r * x_jitter) % 1.0
    y = ((index / (phi * phi)) + r * y_jitter) % 1.0
    # x = (index / phi) % 1.0 + r * numpy.random.rand()
    # y = (index / (phi * phi)) % 1.0 + r * numpy.random.rand()
    return x, y


def lds_r2_jittered_opt(index):
    jitter_amount = 3.0
    r = jitter_amount * 0.336766231 / numpy.sqrt(index - 0.7)
    x_jitter = r * ((numpy.sin(index * 12.9898) * 43758.5453) % 1.0)
    y_jitter = r * ((numpy.sin(index * 78.2331) * 43758.5453) % 1.0)

    phi_inv = 1.0 / 1.324717957244
    x = ((index * phi_inv) + x_jitter) % 1.0
    y = ((index * phi_inv * phi_inv) + y_jitter) % 1.0

    return x, y


def lds_r3(index):
    phi = 1.22074408460575;
    x = (index / phi) % 1.0
    y = (index / (phi * phi)) % 1.0
    z = (index / (phi * phi * phi)) % 1.0
    return x, y, z


def sample_disk(u, v):
    r = u ** 0.5
    phi = 2.0 * numpy.pi * v
    x = r * numpy.cos(phi)
    y = r * numpy.sin(phi)
    return x, y


def sample_disk2(u, v, w):
    r = w ** 0.5
    p = numpy.array([u, v]) - 0.5
    p_length = (p[0] * p[0] + p[1] * p[1]) ** 0.5
    x = r * p[0] / p_length
    y = r * p[1] / p_length
    return x, y


def main():
    N = 4000
    xs = []
    ys = []
    for i in xrange(1, N + 1):
        x, y = lds_r2_jittered_opt(i)
        x, y = sample_disk(x, y)
        xs.append(x)
        ys.append(y)

    colors = numpy.zeros((N, 3))
    colors[0:int(N / 3.0), :] = [0.8, 0.0, 0.2]
    colors[int(N / 3.0):int(2.0 * N / 3.0), :] = [0.0, 0.8, 0.2]
    colors[int(2.0 * N / 3.0):N, :] = [0.0, 0.2, 0.8]

    pylab.scatter(xs, ys, s=5, c=colors)
    pylab.xlim((-1.0, 1.0))
    pylab.ylim((-1.0, 1.0))
    pylab.axes().set_aspect("equal")
    pylab.show()


if __name__ == "__main__":
    main()
