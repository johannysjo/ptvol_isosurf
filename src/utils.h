/// @file
/// @brief Utility functions.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include "gfx.h"

#include <GL/glew.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <stdint.h>
#include <string>
#include <vector>

namespace isopt {

enum MaterialType : int32_t { MATTE = 0, METAL = 1, GLOSSY = 2, GLASS = 3 };

struct Material {
    MaterialType type;
    glm::vec3 base_color;
    float roughness;
    float ior;
    float attenuation;
};

struct SkyLight {
    glm::vec3 ground_color;
    glm::vec3 sky_color;
    float intensity;
};

struct CameraExposureInfo {
    float aperture;
    float shutter_time;
    float iso;
    float exposure_compensation;
};

float compute_exposure(const CameraExposureInfo &exposure_info);

struct Camera {
    glm::vec3 eye;
    glm::vec3 center;
    glm::vec3 up;
    float fovy;
    float aspect;
    float z_near;
    float z_far;
    float lens_radius;
    float focus_distance;
};

void fit_frustum_to_bsphere(const float bsphere_radius, const glm::vec3 &bsphere_center,
                            Camera &camera);

struct PanController {
    glm::vec2 origin = {0.0f, 0.0f};
    glm::vec3 translation = {0.0f, 0.0f, 0.0f};
    bool panning = false;
};

void pan_controller_start(PanController &pan_controller, const glm::vec2 &point);

void pan_controller_pan(PanController &pan_controller, const glm::vec2 &point, const Camera &camera,
                        const glm::ivec4 &viewport);

void pan_controller_stop(PanController &pan_controller);

void pan_controller_reset(PanController &pan_controller);

GLuint load_shader_program(const std::string &compute_shader_filename);

gfx::Texture3D create_uint8_volume_texture(const std::vector<uint8_t> &voxel_data,
                                           const glm::uvec3 &dimensions, GLuint min_filter,
                                           GLuint mag_filter, uint32_t max_level = 0);

gfx::Texture2D load_hdr_texture(const std::string &filename);

gfx::Texture2DArray load_rgba16_png_texture_array(const std::vector<std::string> &filenames);

} // namespace isopt
