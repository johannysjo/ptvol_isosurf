#pragma once

#include <stdint.h>
#include <vector>

namespace isopt {

std::vector<float> generate_permuted_halton_sequences(uint32_t num_sequences, uint32_t num_samples,
                                                      uint32_t seed);

} // namespace isopt
