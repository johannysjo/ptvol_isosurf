#pragma once

#include <stdint.h>

namespace gfx { // forward declarations
struct Texture3D;
} // namespace gfx

namespace isopt {

struct VolumeUInt8; // forward declaration

uint32_t get_max_num_mipmap_levels(uint32_t width, uint32_t height, uint32_t depth);

void generate_max_mipmap(const VolumeUInt8 &volume, uint32_t max_level, gfx::Texture3D &texture);

} // namespace isopt
