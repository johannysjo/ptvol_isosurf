/// @file
/// @brief Minimal logging system implemented with plain macros. Not thread safe!
///
/// @section LICENSE
///
/// Copyright (C) 2021  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <cstdio>

#define LOG_LEVEL_NOTSET 0
#define LOG_LEVEL_DEBUG 1
#define LOG_LEVEL_INFO 2
#define LOG_LEVEL_WARNING 3
#define LOG_LEVEL_ERROR 4

#ifndef LOG_LEVEL
#define LOG_LEVEL LOG_LEVEL_DEBUG
#endif

#if LOG_LEVEL <= LOG_LEVEL_DEBUG
#define LOG_DEBUG(...) std::fprintf(stdout, "DEBUG:" __VA_ARGS__)
#else
#define LOG_DEBUG(...)
#endif

#if LOG_LEVEL <= LOG_LEVEL_INFO
#define LOG_INFO(...) std::fprintf(stdout, "INFO:" __VA_ARGS__)
#else
#define LOG_INFO(...)
#endif

#if LOG_LEVEL <= LOG_LEVEL_WARNING
#define LOG_WARNING(...) std::fprintf(stderr, "WARNING:" __VA_ARGS__)
#else
#define LOG_WARNING(...)
#endif

#if LOG_LEVEL <= LOG_LEVEL_ERROR
#define LOG_ERROR(...) std::fprintf(stderr, "ERROR:" __VA_ARGS__)
#else
#define LOG_ERROR(...)
#endif