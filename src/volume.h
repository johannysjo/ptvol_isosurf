#pragma once

#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace isopt {

struct VolumeUInt8 {
    glm::uvec3 dimensions;
    glm::vec3 spacing;
    std::vector<uint8_t> data;
};

bool load_uint8_vtk_volume(const char *filename, VolumeUInt8 &volume);

void create_max_blocks(const VolumeUInt8 &volume, const glm::uvec3 &num_blocks,
                       VolumeUInt8 &max_blocks);

} // namespace isopt
