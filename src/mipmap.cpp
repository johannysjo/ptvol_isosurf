#include "mipmap.h"

#include "gfx.h"
#include "volume.h"

#include <glm/vec3.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <vector>
#include <omp.h>

namespace {

inline size_t clamp(const size_t x, const size_t min_val, const size_t max_val)
{
    assert(min_val <= max_val);
    return x < min_val ? min_val : (x > max_val ? max_val : x);
}

inline size_t imax(const size_t x, const size_t y)
{
    return x > y ? x : y;
}

inline uint32_t imax(const uint32_t x, const uint32_t y)
{
    return x > y ? x : y;
}

void compute_max_mipmap_level(const isopt::VolumeUInt8 &src_volume, isopt::VolumeUInt8 &dst_volume)
{
    assert(src_volume.dimensions[0] > 0);
    assert(src_volume.dimensions[1] > 0);
    assert(src_volume.dimensions[2] > 0);

    const size_t src_width = src_volume.dimensions[0];
    const size_t src_height = src_volume.dimensions[1];
    const size_t src_depth = src_volume.dimensions[2];
    assert(src_width * src_height * src_depth == src_volume.data.size());

    const size_t dst_width = imax(1, src_width / 2);
    const size_t dst_height = imax(1, src_height / 2);
    const size_t dst_depth = imax(1, src_depth / 2);

    dst_volume.dimensions = glm::uvec3(dst_width, dst_height, dst_depth);
    dst_volume.spacing = 2.0f * src_volume.spacing;
    dst_volume.data.resize(dst_width * dst_height * dst_depth);

    #pragma omp parallel for schedule(dynamic, 1)
    for (size_t k = 0; k < dst_depth; ++k) {
        for (size_t j = 0; j < dst_height; ++j) {
            for (size_t i = 0; i < dst_width; ++i) {
                const size_t ii_min = 2 * i;
                const size_t ii_max = clamp(ii_min + 1, 0, src_width - 1);
                const size_t jj_min = 2 * j;
                const size_t jj_max = clamp(jj_min + 1, 0, src_height - 1);
                const size_t kk_min = 2 * k;
                const size_t kk_max = clamp(kk_min + 1, 0, src_depth - 1);

                uint8_t max_intensity = 0;
                for (size_t kk = kk_min; kk <= kk_max; ++kk) {
                    for (size_t jj = jj_min; jj <= jj_max; ++jj) {
                        for (size_t ii = ii_min; ii <= ii_max; ++ii) {
                            const size_t src_voxel_index =
                                ii + jj * src_width + kk * src_width * src_height;
                            const uint8_t intensity = src_volume.data[src_voxel_index];
                            max_intensity = intensity > max_intensity ? intensity : max_intensity;
                        }
                    }
                }

                const size_t dst_voxel_index = i + j * dst_width + k * dst_width * dst_height;
                dst_volume.data[dst_voxel_index] = max_intensity;
            }
        }
    }
}

} // namespace

namespace isopt {

// Calculates the maximum number of mipmap levels for a 3D texture as specified
// in chapter 8.14.3 of the OpenGL 4.6 specification
uint32_t get_max_num_mipmap_levels(const uint32_t width, const uint32_t height,
                                   const uint32_t depth)
{
    const uint32_t max_size = imax(width, imax(height, depth));
    assert(max_size > 0);
    return uint32_t(std::floor(std::log2(max_size))) + 1;
}

// Generates maximum mipmap levels for a 3D texture using the input volume as
// base (0) level
void generate_max_mipmap(const VolumeUInt8 &volume, const uint32_t max_level,
                         gfx::Texture3D &texture)
{
    // Generate mipmap levels 1 to N
    std::vector<VolumeUInt8> mipmap_volumes;
    for (uint32_t i = 0; i < max_level; ++i) {
        VolumeUInt8 mipmap_volume;
        if (i == 0) {
            compute_max_mipmap_level(volume, mipmap_volume);
        }
        else {
            compute_max_mipmap_level(mipmap_volumes[i - 1], mipmap_volume);
        }
        mipmap_volumes.push_back(mipmap_volume);
    }

    // Upload the mipmap levels to the 3D texture. It is assumed that the base
    // level has already been allocated and uploaded.
    for (uint32_t i = 0; i < mipmap_volumes.size(); ++i) {
        const uint32_t level = i + 1;
        gfx::texture_3d_upload_mipmap_level(
            texture, level, mipmap_volumes[i].dimensions[0], mipmap_volumes[i].dimensions[1],
            mipmap_volumes[i].dimensions[2], mipmap_volumes[i].data.data());
    }
}

} // namespace isopt
