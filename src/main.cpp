/// @file
/// @brief Volumetric path tracer for isosurface rendering.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#include "gfx.h"
#include "gfx_utils.h"
#include "halton.h"
#include "logging.h"
#include "mipmap.h"
#include "utils.h"
#include "volume.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>

enum ToneMappingOp : uint32_t {
    TONE_MAPPING_OP_ACES,
    TONE_MAPPING_OP_UNREAL,
    TONE_MAPPING_OP_GAMMA_ONLY,
    TONE_MAPPING_OP_NONE
};

enum TimeStamp {
    TIME_STAMP_PATH_TRACING,
    TIME_STAMP_DENOISING,
    TIME_STAMP_TAA,
    TIME_STAMP_BLOOM,
    TIME_STAMP_TONE_MAPPING,
    TIME_STAMP_BLIT,
    TIME_STAMP_TOTAL,
    NUM_TIME_STAMPS
};

struct Window {
    GLFWwindow *handle = nullptr;
    uint32_t width = 800;
    uint32_t height = 600;
    uint32_t frame_index = 1;
    uint32_t total_frame_index = 1;
};

struct BloomSettings {
    bool enabled = true;
    uint32_t downsampling = 4;
    uint32_t num_iterations = 2;
    float intensity = 0.05f;
};

struct Settings {
    float step_size_voxels = 1.0f;
    uint32_t spp = 1000;
    uint32_t min_spp = 1;
    uint32_t max_spp = 2000;
    uint32_t path_depth = 5;
    uint32_t min_path_depth = 1;
    uint32_t max_path_depth = 10;
    float isovalue = 0.5f;
    bool clamping_enabled = true;
    float clamping_threshold = 10.0f;
    float sky_light_intensity = 30000.0f;
    bool show_envmap = true;
    float exposure = 1.0f;
    bool denoising_enabled = false;
    bool taa_enabled = false;
    BloomSettings bloom;
    ToneMappingOp tone_mapping_op = TONE_MAPPING_OP_ACES;
};

struct UISettings {
    bool show_settings_ui = true;
    ImVec2 settings_ui_size = {260, 560};
    ImVec2 settings_ui_pos = {5, 5};
    bool settings_ui_collapsed = false;

    bool show_progress_ui = true;
    ImVec2 progress_ui_size = {200, 55};
    ImVec2 progress_ui_pos = {settings_ui_size.x + settings_ui_pos.x + 5, 5};
    bool progress_ui_collapsed = false;

    bool show_profiler_ui = true;
    ImVec2 profiler_ui_size = {270, 195};
    ImVec2 profiler_ui_pos = {progress_ui_size.x + progress_ui_pos.x + 5, 5};
    bool profiler_ui_collapsed = true;
};

struct Context {
    Window window;
    GLuint timer_queries_tic[NUM_TIME_STAMPS];
    GLuint timer_queries_toc[NUM_TIME_STAMPS];
    float frame_times_ms[NUM_TIME_STAMPS];

    isopt::Camera camera;
    isopt::CameraExposureInfo camera_exposure_info;
    gfx::Trackball trackball;
    isopt::PanController pan_controller;

    GLuint default_vao = 0;
    std::unordered_map<std::string, GLuint> programs;
    std::unordered_map<std::string, gfx::FBO> fbos;

    std::string volume_filename = "";
    isopt::VolumeUInt8 volume;
    gfx::Texture3D volume_texture;
    isopt::VolumeUInt8 max_blocks;
    gfx::Texture3D max_block_texture;
    isopt::Material material;
    glm::mat4 world_from_model;
    glm::mat4 model_from_box;

    gfx::Texture2DArray blue_noise_texture;
    gfx::Texture2D halton_texture;
    gfx::Texture2D envmap_texture;

    Settings settings;
    UISettings ui;
};

std::string root_dir()
{
    const char *path = std::getenv("ISOPATH_ROOT");
    const auto path_str = path != nullptr ? std::string(path) : std::string();
    if (path_str.empty()) {
        LOG_ERROR("ISOPATH_ROOT is not set.\n");
        std::exit(EXIT_FAILURE);
    }

    return path_str;
}

void load_reload_shaders(Context &ctx)
{
    const std::string shader_dir = root_dir() + "/src/shaders/";

    ctx.programs["path_tracer"] = isopt::load_shader_program(shader_dir + "path_tracer.comp");
    ctx.programs["denoising"] = isopt::load_shader_program(shader_dir + "denoising.comp");
    ctx.programs["taa"] = isopt::load_shader_program(shader_dir + "taa.comp");
    ctx.programs["bloom_brightpass"] =
        isopt::load_shader_program(shader_dir + "bloom_brightpass.comp");
    ctx.programs["gaussian_blur"] = isopt::load_shader_program(shader_dir + "gaussian_blur.comp");
    ctx.programs["bloom_blend"] = isopt::load_shader_program(shader_dir + "bloom_blend.comp");
    ctx.programs["tone_mapping"] = isopt::load_shader_program(shader_dir + "tone_mapping.comp");
}

void init_fbos(Context &ctx)
{
    { // Path tracing
        gfx::Texture2D ptrace_texture;
        ptrace_texture.target = GL_TEXTURE_2D;
        ptrace_texture.width = ctx.window.width;
        ptrace_texture.height = ctx.window.height;
        ptrace_texture.storage = {GL_RGBA32F, GL_RGBA, GL_FLOAT, 1};
        ptrace_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(ptrace_texture);

        gfx::Texture2D depth_texture;
        depth_texture.target = GL_TEXTURE_2D;
        depth_texture.width = ctx.window.width;
        depth_texture.height = ctx.window.height;
        depth_texture.storage = {GL_R32F, GL_RED, GL_FLOAT, 1};
        depth_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(depth_texture);

        gfx::Texture2D world_normal_texture;
        world_normal_texture.target = GL_TEXTURE_2D;
        world_normal_texture.width = ctx.window.width;
        world_normal_texture.height = ctx.window.height;
        world_normal_texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
        world_normal_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(world_normal_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, ptrace_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, depth_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT2, world_normal_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["ptrace"] = fbo;
    }

    { // Scene
        gfx::Texture2D scene_texture;
        scene_texture.target = GL_TEXTURE_2D;
        scene_texture.width = ctx.window.width;
        scene_texture.height = ctx.window.height;
        scene_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        scene_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(scene_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, scene_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["scene"] = fbo;
    }

    { // Scene2
        gfx::Texture2D scene2_texture;
        scene2_texture.target = GL_TEXTURE_2D;
        scene2_texture.width = ctx.window.width;
        scene2_texture.height = ctx.window.height;
        scene2_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        scene2_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(scene2_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, scene2_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["scene2"] = fbo;
    }

    { // TAA
        gfx::Texture2D history_texture;
        history_texture.target = GL_TEXTURE_2D;
        history_texture.width = ctx.window.width;
        history_texture.height = ctx.window.height;
        history_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        history_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(history_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, history_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["taa"] = fbo;
    }

    { // Bloom
        const uint32_t width = ctx.window.width / ctx.settings.bloom.downsampling;
        const uint32_t height = ctx.window.height / ctx.settings.bloom.downsampling;

        gfx::Texture2D bloom_texture;
        bloom_texture.target = GL_TEXTURE_2D;
        bloom_texture.width = width;
        bloom_texture.height = height;
        bloom_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        bloom_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(bloom_texture);

        gfx::Texture2D bloom2_texture;
        bloom2_texture.target = GL_TEXTURE_2D;
        bloom2_texture.width = width;
        bloom2_texture.height = height;
        bloom2_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        bloom2_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(bloom2_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, width, height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, bloom_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, bloom2_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["bloom"] = fbo;
    }

    { // Tone mapping
        gfx::Texture2D color_texture;
        color_texture.target = GL_TEXTURE_2D;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
        color_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture);

        gfx::FBO fbo;
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["tone_mapping"] = fbo;
    }
}

void load_volume(Context &ctx)
{
    // Load volume image
    if (!load_uint8_vtk_volume(ctx.volume_filename.c_str(), ctx.volume)) {
        LOG_ERROR("Could not load volume from %s\n", ctx.volume_filename.c_str());
        std::exit(EXIT_FAILURE);
    }

    // Generate volume texture and mipmap
    const uint32_t num_mipmap_levels = isopt::get_max_num_mipmap_levels(
        ctx.volume.dimensions[0], ctx.volume.dimensions[1], ctx.volume.dimensions[2]);
    assert(num_mipmap_levels > 0);
    const uint32_t max_level = num_mipmap_levels - 1;
    glDeleteTextures(1, &ctx.volume_texture.texture);
    ctx.volume_texture = isopt::create_uint8_volume_texture(
        ctx.volume.data, ctx.volume.dimensions, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, max_level);
    isopt::generate_max_mipmap(ctx.volume, max_level, ctx.volume_texture);

    // Create max block structure for empty-space skipping
    isopt::create_max_blocks(ctx.volume, glm::uvec3(32, 32, 32), ctx.max_blocks);
    glDeleteTextures(1, &ctx.max_block_texture.texture);
    ctx.max_block_texture = isopt::create_uint8_volume_texture(
        ctx.max_blocks.data, ctx.max_blocks.dimensions, GL_NEAREST, GL_NEAREST);

    // Define a transform that scales a 2-unit cube centered at origin to the physical extent of the
    // volume image
    const glm::vec3 volume_extent = glm::vec3(ctx.volume.dimensions) * ctx.volume.spacing;
    ctx.model_from_box = glm::scale(glm::mat4(1.0f), 0.5f * volume_extent);
}

void init_halton_sequence(Context &ctx)
{
    const uint32_t num_base_samples = 4;   // must be kept in sync with path_tracer.comp!
    const uint32_t num_bounce_samples = 3; // must be kept in sync with path_tracer.comp!
    const uint32_t num_sequences =
        num_base_samples + ctx.settings.max_path_depth * num_bounce_samples;
    const uint32_t num_samples_per_sequence = ctx.settings.max_spp;
    const uint32_t seed = 12371;
    const std::vector<float> halton_sequences =
        isopt::generate_permuted_halton_sequences(num_sequences, num_samples_per_sequence, seed);

    gfx::Texture2D texture;
    texture.target = GL_TEXTURE_2D;
    texture.width = num_samples_per_sequence;
    texture.height = num_sequences;
    texture.storage = {GL_R32F, GL_RED, GL_FLOAT, 1};
    texture.sampling = {GL_NEAREST, GL_NEAREST, GL_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, halton_sequences.data());

    ctx.halton_texture = texture;
}

void load_envmap(Context &ctx, const std::string &filename)
{
    glDeleteTextures(1, &ctx.envmap_texture.texture);
    ctx.envmap_texture = isopt::load_hdr_texture(filename);
}

void init_reset_camera(Context &ctx)
{
    ctx.camera.eye = glm::vec3(0.0f, 0.0f, 600.0f);
    ctx.camera.center = glm::vec3(0.0f, 0.0f, 0.0f);
    ctx.camera.up = glm::vec3(0.0f, 1.0f, 0.0f);
    ctx.camera.fovy = glm::radians(45.0f);
    ctx.camera.aspect = float(ctx.window.width) / ctx.window.height;
    ctx.camera.z_near = 1.0f;
    ctx.camera.z_far = 1000.0f;
    ctx.camera.lens_radius = 5.0f;
    ctx.camera.focus_distance = 550.0f;

    const glm::vec3 volume_extent = glm::vec3(ctx.volume.dimensions) * ctx.volume.spacing;
    const float bsphere_radius = glm::length(-0.5f * volume_extent);
    const glm::vec3 bsphere_center = {0.0f, 0.0f, 0.0f};
    isopt::fit_frustum_to_bsphere(bsphere_radius, bsphere_center, ctx.camera);
}

void init_camera_exposure(Context &ctx)
{
    ctx.camera_exposure_info.aperture = 13.0f;
    ctx.camera_exposure_info.shutter_time = 1.0f / 100.0f;
    ctx.camera_exposure_info.iso = 100.0f;
    ctx.camera_exposure_info.exposure_compensation = 0.0f;
}

void init_timer_queries(Context &ctx)
{
    glGenQueries(NUM_TIME_STAMPS, ctx.timer_queries_tic);
    glGenQueries(NUM_TIME_STAMPS, ctx.timer_queries_toc);
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        ctx.frame_times_ms[i] = 0.0f;
    }
}

void restart_path_tracing(Context &ctx)
{
    ctx.window.frame_index = 1;
}

void init(Context &ctx)
{
    glGenVertexArrays(1, &ctx.default_vao);
    load_reload_shaders(ctx);
    init_fbos(ctx);

    load_volume(ctx);

    ctx.material.type = isopt::GLOSSY;
    ctx.material.base_color = glm::vec3(0.7f, 0.55f, 0.25f);
    ctx.material.roughness = 0.2f;
    ctx.material.ior = 1.5f;
    ctx.material.attenuation = 1.0f;

    init_halton_sequence(ctx);

    const std::vector<std::string> blue_noise_filenames = {
        root_dir() + "/resources/blue_noise_textures/64_64/HDR_RGBA_0.png",
        root_dir() + "/resources/blue_noise_textures/64_64/HDR_RGBA_1.png"};
    ctx.blue_noise_texture = isopt::load_rgba16_png_texture_array(blue_noise_filenames);

    load_envmap(ctx, root_dir() + "/resources/envmaps/abandoned_slipway_2k.hdr");

    init_reset_camera(ctx);
    init_camera_exposure(ctx);
    init_timer_queries(ctx);
}

void update_transforms(Context &ctx)
{
    ctx.world_from_model =
        glm::translate(glm::mat4(1.0f), ctx.pan_controller.translation) *
        gfx::trackball_get_rotation_matrix(ctx.trackball) *
        glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
}

void update_settings_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui.settings_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui.settings_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui.settings_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(0.7f);
    ImGui::Begin("Settings", &ctx.ui.show_settings_ui);

    if (ImGui::CollapsingHeader("Volume rendering")) {
        if (ImGui::SliderFloat("Step size (voxels)", &ctx.settings.step_size_voxels, 0.25f, 2.0f)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderInt("SPP", (int32_t *)&ctx.settings.spp, ctx.settings.min_spp,
                             ctx.settings.max_spp)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderInt("Path depth", (int32_t *)&ctx.settings.path_depth,
                             ctx.settings.min_path_depth, ctx.settings.max_path_depth)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderFloat("Isovalue", &ctx.settings.isovalue, 0.0f, 1.0f)) {
            restart_path_tracing(ctx);
        }
    }

    if (ImGui::CollapsingHeader("Material")) {
        if (ImGui::Combo("Type", (int32_t *)(&ctx.material.type),
                         "Matte\0Metal\0Glossy\0Glass\0")) {
            restart_path_tracing(ctx);
        }
        if (ImGui::ColorEdit3("Base color", &ctx.material.base_color[0])) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderFloat("Roughness", &ctx.material.roughness, 0.0f, 1.0f)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderFloat("IOR", &ctx.material.ior, 1.0f, 3.0f)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderFloat("Attenuation", &ctx.material.attenuation, 0.001f, 1.0f)) {
            restart_path_tracing(ctx);
        }
    }

    if (ImGui::CollapsingHeader("Lighting")) {
        if (ImGui::SliderFloat("Sky light intensity (lux)", &ctx.settings.sky_light_intensity, 0.0f,
                               100000.0f)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::Checkbox("Show envmap", &ctx.settings.show_envmap)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::Checkbox("Clamping", &ctx.settings.clamping_enabled)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderFloat("Clamping threshold", &ctx.settings.clamping_threshold, 1.0f,
                               50.0f)) {
            restart_path_tracing(ctx);
        }
    }

    if (ImGui::CollapsingHeader("Camera")) {
        if (ImGui::SliderFloat("Aperture", &ctx.camera_exposure_info.aperture, 1.0f, 22.0f)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderFloat("Shutter time (s)", &ctx.camera_exposure_info.shutter_time, 0.001f,
                               1.0f)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderFloat("ISO", &ctx.camera_exposure_info.iso, 50.0f, 1600.0f)) {
            restart_path_tracing(ctx);
        }
        ImGui::SliderFloat("EC (-/+)", &ctx.camera_exposure_info.exposure_compensation, -4.0f,
                           4.0f);
        if (ImGui::SliderFloat("Focus distance", &ctx.camera.focus_distance, 0.0f, 2000.0f)) {
            restart_path_tracing(ctx);
        }
        if (ImGui::SliderFloat("Lens radius", &ctx.camera.lens_radius, 0.0f, 40.0f)) {
            restart_path_tracing(ctx);
        }
    }

    if (ImGui::CollapsingHeader("PostFX")) {
        ImGui::Checkbox("Denoising", &ctx.settings.denoising_enabled);
        ImGui::Checkbox("TAA", &ctx.settings.taa_enabled);
        ImGui::Checkbox("Bloom", &ctx.settings.bloom.enabled);
        ImGui::SliderFloat("Bloom intensity", &ctx.settings.bloom.intensity, 0.0f, 1.0f);
        ImGui::Combo("Tone mapping operator", (int32_t *)(&ctx.settings.tone_mapping_op),
                     "Aces\0Unreal\0Gamma-only\0None\0");
    }

    if (ImGui::CollapsingHeader("Info")) {
        ImGui::Text("Viewport resolution: %dx%d", ctx.window.width, ctx.window.height);
        ImGui::Text("Volume dimensions: %dx%dx%d", ctx.volume.dimensions[0],
                    ctx.volume.dimensions[1], ctx.volume.dimensions[2]);
        ImGui::Text("Voxel size: %.3f %.3f %.3f", ctx.volume.spacing[0], ctx.volume.spacing[1],
                    ctx.volume.spacing[2]);
    }

    ImGui::End();
}

void update_progress_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui.progress_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui.progress_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui.progress_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(0.7f);
    ImGui::Begin("Progress", &ctx.ui.show_progress_ui);

    float progress = float(ctx.window.frame_index) / ctx.settings.spp;
    progress = std::fmax(0.0f, std::fmin(1.0f, progress));
    ImGui::ProgressBar(progress);

    ImGui::End();
}

void profiler_bar(const char *label, const float time_in_ms, const ImVec4 &color)
{
    const float cursor_pos_x = ImGui::GetCursorPosX();
    ImGui::ColorButton("", color, 0, ImVec2(std::fmin(10.0f * time_in_ms, 1000.0f), 0.0f));
    ImGui::SameLine();
    ImGui::SetCursorPosX(cursor_pos_x + 2.0f);
    ImGui::Text("%s %.1f", label, time_in_ms);
}

void update_profiler_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui.profiler_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui.profiler_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui.profiler_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(0.7f);
    ImGui::Begin("Frame times (ms)", &ctx.ui.show_profiler_ui);

    const ImVec4 color = {0.7f, 0.5f, 0.1f, 1.0f};
    profiler_bar("Path-tracing", ctx.frame_times_ms[TIME_STAMP_PATH_TRACING], color);
    profiler_bar("Denoising", ctx.frame_times_ms[TIME_STAMP_DENOISING], color);
    profiler_bar("TAA", ctx.frame_times_ms[TIME_STAMP_TAA], color);
    profiler_bar("Bloom", ctx.frame_times_ms[TIME_STAMP_BLOOM], color);
    profiler_bar("Tone mapping", ctx.frame_times_ms[TIME_STAMP_TONE_MAPPING], color);
    profiler_bar("Blit", ctx.frame_times_ms[TIME_STAMP_BLIT], color);
    profiler_bar("Total", ctx.frame_times_ms[TIME_STAMP_TOTAL], color);

    ImGui::End();
}

void update(Context &ctx)
{
    update_transforms(ctx);
    update_settings_ui(ctx);
    update_progress_ui(ctx);
    update_profiler_ui(ctx);
    ctx.settings.exposure = isopt::compute_exposure(ctx.camera_exposure_info);
}

void dispatch_path_tracing(
    const GLuint program, const gfx::Texture3D &volume_texture,
    const gfx::Texture3D &max_block_texture, const gfx::Texture2D &halton_texture,
    const gfx::Texture2DArray &blue_noise_texture, const gfx::Texture2D &envmap_texture,
    const gfx::Texture2D &ptrace_accum_texture, const gfx::Texture2D &first_hit_depth_texture,
    const gfx::Texture2D &first_hit_world_normal_texture, const glm::vec3 &volume_spacing,
    const glm::mat4 &model_from_box, const Context &ctx)
{
    const glm::mat4 world_from_model = ctx.world_from_model * model_from_box;
    const glm::mat4 model_from_world = glm::inverse(world_from_model);
    const glm::mat3 world_normal_from_model =
        glm::transpose(glm::inverse(glm::mat3(world_from_model)));

    glBindTextureUnit(0, volume_texture.texture);
    glBindTextureUnit(1, max_block_texture.texture);
    glBindTextureUnit(2, halton_texture.texture);
    glBindTextureUnit(3, blue_noise_texture.texture);
    glBindTextureUnit(4, envmap_texture.texture);
    glBindImageTexture(0, ptrace_accum_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
    glBindImageTexture(1, first_hit_depth_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32F);
    glBindImageTexture(2, first_hit_world_normal_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY,
                       GL_RGBA8);

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &world_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &model_from_world[0][0]);
    glProgramUniformMatrix3fv(program, 2, 1, GL_FALSE, &world_normal_from_model[0][0]);
    glProgramUniform1f(program, 3, ctx.settings.step_size_voxels);
    glProgramUniform3fv(program, 4, 1, &volume_spacing[0]);
    glProgramUniform1f(program, 5, ctx.settings.isovalue);
    glProgramUniform1ui(program, 6, ctx.settings.spp);
    glProgramUniform1ui(program, 7, ctx.settings.path_depth);
    glProgramUniform1i(program, 8, ctx.window.frame_index);
    glProgramUniform1f(program, 9, ctx.settings.exposure);
    glProgramUniform1i(program, 10, ctx.material.type);
    glProgramUniform3fv(program, 11, 1, &ctx.material.base_color[0]);
    glProgramUniform1f(program, 12, ctx.material.roughness);
    glProgramUniform1f(program, 13, ctx.material.ior);
    glProgramUniform1f(program, 14, ctx.material.attenuation);
    glProgramUniform1fv(program, 15, 1, &ctx.settings.sky_light_intensity);
    glProgramUniform1i(program, 16, ctx.window.total_frame_index);
    glProgramUniform3fv(program, 17, 1, &ctx.camera.eye[0]);
    glProgramUniform3fv(program, 18, 1, &ctx.camera.center[0]);
    glProgramUniform3fv(program, 19, 1, &ctx.camera.up[0]);
    glProgramUniform1f(program, 20, ctx.camera.fovy);
    glProgramUniform1f(program, 21, ctx.camera.aspect);
    glProgramUniform1f(program, 22, ctx.camera.lens_radius);
    glProgramUniform1f(program, 23, ctx.camera.focus_distance);
    glProgramUniform1ui(program, 24, ctx.settings.clamping_enabled);
    glProgramUniform1f(program, 25, ctx.settings.clamping_threshold);
    glProgramUniform1ui(program, 26, ctx.settings.show_envmap);

    glUseProgram(program);
    glDispatchCompute(ptrace_accum_texture.width / 8 + 1, ptrace_accum_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_denoising(const GLuint program, const gfx::Texture2D &color_texture,
                        const gfx::Texture2D &depth_texture, const gfx::Texture2D &normal_texture,
                        const gfx::Texture2D &output_texture, const int frame_index,
                        const glm::vec2 &direction)
{
    glBindTextureUnit(0, color_texture.texture);
    glBindTextureUnit(1, depth_texture.texture);
    glBindTextureUnit(2, normal_texture.texture);
    glBindImageTexture(3, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);
    glProgramUniform1i(program, 0, frame_index);
    glProgramUniform2fv(program, 1, 1, &direction[0]);
    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_taa(const GLuint program, const gfx::Texture2D &current_texture,
                  const gfx::Texture2D &history_texture, const int frame_index)
{
    glBindTextureUnit(0, current_texture.texture);
    glBindImageTexture(1, history_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16F);
    glProgramUniform1i(program, 0, frame_index);
    glUseProgram(program);
    glDispatchCompute(history_texture.width / 8 + 1, history_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_bloom_brightpass(const GLuint program, const gfx::Texture2D &input_texture,
                               const gfx::Texture2D &output_texture, const float intensity)
{
    glBindTextureUnit(0, input_texture.texture);
    glBindImageTexture(1, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R11F_G11F_B10F);
    glProgramUniform1f(program, 0, intensity);
    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_gaussian_blur(const GLuint program, const gfx::Texture2D &texture,
                            const gfx::Texture2D &output_texture, const glm::vec3 &direction)
{
    glBindTextureUnit(0, texture.texture);
    glBindImageTexture(1, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R11F_G11F_B10F);
    glProgramUniform3fv(program, 0, 1, &direction[0]);
    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_bloom_blend(const GLuint program, const gfx::Texture2D &bloom_texture,
                          const gfx::Texture2D &scene_texture)
{
    glBindTextureUnit(0, bloom_texture.texture);
    glBindImageTexture(1, scene_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16F);
    glUseProgram(program);
    glDispatchCompute(scene_texture.width / 8 + 1, scene_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_tone_mapping(const GLuint program, const gfx::Texture2D &color_texture,
                           const gfx::Texture2D &output_texture, const uint32_t tone_mapping_op,
                           const float exposure_compensation)
{
    glBindTextureUnit(0, color_texture.texture);
    glBindImageTexture(1, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
    glProgramUniform1ui(program, 0, tone_mapping_op);
    glProgramUniform1f(program, 1, exposure_compensation);
    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void update_frame_times(Context &ctx)
{
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        GLuint64 tic_ns;
        glGetQueryObjectui64v(ctx.timer_queries_tic[i], GL_QUERY_RESULT, &tic_ns);
        GLuint64 toc_ns;
        glGetQueryObjectui64v(ctx.timer_queries_toc[i], GL_QUERY_RESULT, &toc_ns);
        const float elapsed_time_ms = toc_ns > tic_ns ? float(toc_ns - tic_ns) / 1.0e6f : 0.0f;
        ctx.frame_times_ms[i] = 0.2f * elapsed_time_ms + 0.8f * ctx.frame_times_ms[i];
    }
}

void display(Context &ctx)
{
    glBindVertexArray(ctx.default_vao);

    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TOTAL], GL_TIMESTAMP);

    // Execute path tracer
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_PATH_TRACING], GL_TIMESTAMP);
    {
        dispatch_path_tracing(ctx.programs["path_tracer"], ctx.volume_texture,
                              ctx.max_block_texture, ctx.halton_texture, ctx.blue_noise_texture,
                              ctx.envmap_texture,
                              ctx.fbos["ptrace"].attachments[GL_COLOR_ATTACHMENT0],
                              ctx.fbos["ptrace"].attachments[GL_COLOR_ATTACHMENT1],
                              ctx.fbos["ptrace"].attachments[GL_COLOR_ATTACHMENT2],
                              ctx.volume.spacing, ctx.model_from_box, ctx);

        glNamedFramebufferReadBuffer(ctx.fbos["ptrace"].fbo, GL_COLOR_ATTACHMENT0);
        glNamedFramebufferDrawBuffer(ctx.fbos["scene"].fbo, GL_COLOR_ATTACHMENT0);
        glBlitNamedFramebuffer(ctx.fbos["ptrace"].fbo, ctx.fbos["scene"].fbo, 0, 0,
                               ctx.window.width, ctx.window.height, 0, 0, ctx.window.width,
                               ctx.window.height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_PATH_TRACING], GL_TIMESTAMP);

    // Denoise the path tracing result
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_DENOISING], GL_TIMESTAMP);
    if (ctx.settings.denoising_enabled) {
        std::swap(ctx.fbos["scene"], ctx.fbos["scene2"]);
        dispatch_denoising(ctx.programs["denoising"],
                           ctx.fbos["scene2"].attachments[GL_COLOR_ATTACHMENT0],
                           ctx.fbos["ptrace"].attachments[GL_COLOR_ATTACHMENT1],
                           ctx.fbos["ptrace"].attachments[GL_COLOR_ATTACHMENT2],
                           ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
                           ctx.window.frame_index, glm::vec2(1.0f, 0.0f));

        std::swap(ctx.fbos["scene"], ctx.fbos["scene2"]);
        dispatch_denoising(ctx.programs["denoising"],
                           ctx.fbos["scene2"].attachments[GL_COLOR_ATTACHMENT0],
                           ctx.fbos["ptrace"].attachments[GL_COLOR_ATTACHMENT1],
                           ctx.fbos["ptrace"].attachments[GL_COLOR_ATTACHMENT2],
                           ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
                           ctx.window.frame_index, glm::vec2(0.0f, 1.0f));
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_DENOISING], GL_TIMESTAMP);

    // Apply TAA
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TAA], GL_TIMESTAMP);
    if (ctx.settings.taa_enabled) {
        dispatch_taa(ctx.programs["taa"], ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
                     ctx.fbos["taa"].attachments[GL_COLOR_ATTACHMENT0], ctx.window.frame_index);

        glNamedFramebufferReadBuffer(ctx.fbos["taa"].fbo, GL_COLOR_ATTACHMENT0);
        glNamedFramebufferDrawBuffer(ctx.fbos["scene"].fbo, GL_COLOR_ATTACHMENT0);
        glBlitNamedFramebuffer(ctx.fbos["taa"].fbo, ctx.fbos["scene"].fbo, 0, 0, ctx.window.width,
                               ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                               GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }
    else {
        glNamedFramebufferReadBuffer(ctx.fbos["scene"].fbo, GL_COLOR_ATTACHMENT0);
        glNamedFramebufferDrawBuffer(ctx.fbos["taa"].fbo, GL_COLOR_ATTACHMENT0);
        glBlitNamedFramebuffer(ctx.fbos["scene"].fbo, ctx.fbos["taa"].fbo, 0, 0, ctx.window.width,
                               ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                               GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TAA], GL_TIMESTAMP);

    // Apply bloom
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_BLOOM], GL_TIMESTAMP);
    if (ctx.settings.bloom.enabled) {
        // Downsample and extract bright regions that should bloom
        dispatch_bloom_brightpass(
            ctx.programs["bloom_brightpass"], ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
            ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0], ctx.settings.bloom.intensity);

        // Blur the bright regions
        for (uint32_t i = 0; i < ctx.settings.bloom.num_iterations; ++i) {
            dispatch_gaussian_blur(
                ctx.programs["gaussian_blur"], ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0],
                ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT1], glm::vec3(1.0f, 0.0f, 0.0f));

            dispatch_gaussian_blur(
                ctx.programs["gaussian_blur"], ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT1],
                ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0], glm::vec3(0.0f, 1.0f, 0.0f));
        }

        // Blend the blurred result with the original scene
        dispatch_bloom_blend(ctx.programs["bloom_blend"],
                             ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0],
                             ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0]);
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_BLOOM], GL_TIMESTAMP);

    // Apply tone mapping
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TONE_MAPPING], GL_TIMESTAMP);
    dispatch_tone_mapping(
        ctx.programs["tone_mapping"], ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
        ctx.fbos["tone_mapping"].attachments[GL_COLOR_ATTACHMENT0], ctx.settings.tone_mapping_op,
        ctx.camera_exposure_info.exposure_compensation);
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TONE_MAPPING], GL_TIMESTAMP);

    // Blit
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_BLIT], GL_TIMESTAMP);
    glNamedFramebufferReadBuffer(ctx.fbos["tone_mapping"].fbo, GL_COLOR_ATTACHMENT0);
    glBlitNamedFramebuffer(ctx.fbos["tone_mapping"].fbo, 0, 0, 0, ctx.window.width,
                           ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                           GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_BLIT], GL_TIMESTAMP);

    // Required for ImGui
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TOTAL], GL_TIMESTAMP);
    update_frame_times(ctx);
}

void error_callback(const int /*error*/, const char *description)
{
    LOG_ERROR("%s\n", description);
}

void key_callback(GLFWwindow *window, const int key, const int scancode, const int action,
                  const int mods)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) {
        return;
    }

    if (key == GLFW_KEY_R && action == GLFW_PRESS) {
        load_reload_shaders(*ctx);
        restart_path_tracing(*ctx);
    }
}

void scroll_callback(GLFWwindow *window, const double x_offset, const double y_offset)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_ScrollCallback(window, x_offset, y_offset);
    if (ImGui::GetIO().WantCaptureMouse) {
        return;
    }

    const float zoom_step = glm::radians(1.0f);
    if (y_offset > 0.0) {
        ctx->camera.fovy = std::fmax(zoom_step, ctx->camera.fovy - zoom_step);
        restart_path_tracing(*ctx);
    }
    else if (y_offset < 0.0) {
        ctx->camera.fovy = std::fmin(180.0f - zoom_step, ctx->camera.fovy + zoom_step);
        restart_path_tracing(*ctx);
    }
}

void mouse_button_pressed(Context &ctx, const int button, const int x, const int y)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        gfx::trackball_start_tracking(ctx.trackball, glm::vec2(x, y));
    }
    else if (button == GLFW_MOUSE_BUTTON_MIDDLE) {
        isopt::pan_controller_start(ctx.pan_controller, glm::vec2(x, y));
    }
}

void mouse_button_released(Context &ctx, const int button, const int /*x*/, const int /*y*/)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        gfx::trackball_stop_tracking(ctx.trackball);
    }
    else if (button == GLFW_MOUSE_BUTTON_MIDDLE) {
        isopt::pan_controller_stop(ctx.pan_controller);
    }
}

void mouse_button_callback(GLFWwindow *window, const int button, const int action, const int mods)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) {
        return;
    }

    double x, y;
    glfwGetCursorPos(window, &x, &y);
    if (action == GLFW_PRESS) {
        mouse_button_pressed(*ctx, button, x, y);
    }
    else {
        mouse_button_released(*ctx, button, x, y);
    }
}

void cursor_pos_callback(GLFWwindow *window, const double x, const double y)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    if (ctx->trackball.tracking) {
        gfx::trackball_move(ctx->trackball, glm::vec2(x, y));
        restart_path_tracing(*ctx);
    }

    if (ctx->pan_controller.panning) {
        const glm::ivec4 viewport = {0, 0, ctx->window.width, ctx->window.height};
        isopt::pan_controller_pan(ctx->pan_controller, glm::vec2(x, y), ctx->camera, viewport);
        restart_path_tracing(*ctx);
    }
}

std::string get_file_extension(const std::string &filename)
{
    const size_t pos = filename.rfind('.');
    return pos != std::string::npos ? filename.substr(pos) : "";
}

void drop_callback(GLFWwindow *window, const int count, const char **paths)
{
    if (count < 1)
        return;

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    const auto filename = std::string(paths[0]);
    const std::string extension = get_file_extension(filename);
    if (extension == ".hdr") {
        load_envmap(*ctx, filename);
        restart_path_tracing(*ctx);
    }
    else if (extension == ".vtk") {
        ctx->volume_filename = filename;
        load_volume(*ctx);
        init_reset_camera(*ctx);
        restart_path_tracing(*ctx);
    }
}

void resize_fbos(Context &ctx, const int width, const int height)
{
    for (auto &fbo : ctx.fbos) {
        gfx::fbo_resize(fbo.second, width, height);
    }

    gfx::fbo_resize(ctx.fbos["bloom"], width / ctx.settings.bloom.downsampling,
                    height / ctx.settings.bloom.downsampling);
}

void framebuffer_size_callback(GLFWwindow *window, const int width, const int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ctx->window.width = width;
    ctx->window.height = height;
    ctx->camera.aspect = float(width) / height;
    resize_fbos(*ctx, width, height);
    glViewport(0, 0, width, height);
    restart_path_tracing(*ctx);
}

int main(int argc, char **argv)
{
    Context ctx;

    ctx.volume_filename = argc >= 2 ? argv[1] : root_dir() + "/data/dragon_256_uint8.vtk";

    // Initialize GLFW
    glfwSetErrorCallback(error_callback);
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    ctx.window.handle =
        glfwCreateWindow(ctx.window.width, ctx.window.height, "ISOPATH", nullptr, nullptr);
    assert(ctx.window.handle != nullptr);
    glfwMakeContextCurrent(ctx.window.handle);

    glfwSetWindowUserPointer(ctx.window.handle, &ctx);
    glfwSetKeyCallback(ctx.window.handle, key_callback);
    glfwSetCursorPosCallback(ctx.window.handle, cursor_pos_callback);
    glfwSetScrollCallback(ctx.window.handle, scroll_callback);
    glfwSetMouseButtonCallback(ctx.window.handle, mouse_button_callback);
    glfwSetDropCallback(ctx.window.handle, drop_callback);
    glfwSetFramebufferSizeCallback(ctx.window.handle, framebuffer_size_callback);

    // Initialize GLEW
    glewExperimental = true;
    GLenum status = glewInit();
    if (status != GLEW_OK) {
        LOG_ERROR("%s\n", glewGetErrorString(status));
        std::exit(EXIT_FAILURE);
    }
    LOG_INFO("OpenGL version: %s\n", glGetString(GL_VERSION));

    // Initialize rendering
    init(ctx);

    // Initialize ImGui
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(ctx.window.handle, false);
    ImGui_ImplOpenGL3_Init("#version 450");

    // Start rendering loop
    while (!glfwWindowShouldClose(ctx.window.handle)) {
        glfwPollEvents();
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        update(ctx);
        display(ctx);
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(ctx.window.handle);
        ctx.window.frame_index += 1;
        ctx.window.total_frame_index += 1;
    }

    // Shut down everything
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    glfwDestroyWindow(ctx.window.handle);
    glfwTerminate();

    return EXIT_SUCCESS;
}
