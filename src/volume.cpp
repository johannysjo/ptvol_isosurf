#include "volume.h"

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <omp.h>

namespace {

inline size_t clamp(const size_t x, const size_t min_val, const size_t max_val)
{
    assert(min_val <= max_val);
    return x < min_val ? min_val : (x > max_val ? max_val : x);
}

} // namespace

namespace isopt {

bool load_uint8_vtk_volume(const char *filename, VolumeUInt8 &volume)
{
    std::FILE *fp = std::fopen(filename, "rb");
    if (fp == nullptr) {
        std::fclose(fp);
        return false;
    }

    // Read header
    const uint32_t num_header_lines = 10;
    const uint32_t max_line_length = 256;
    char lines[num_header_lines][max_line_length];
    for (uint32_t i = 0; i < num_header_lines; ++i) {
        if (std::fgets(lines[i], max_line_length, fp) == nullptr) {
            std::fclose(fp);
            return false;
        }
    }

    if (!(std::strncmp(lines[0], "# vtk", 5) == 0 || std::strncmp(lines[0], "# VTK", 5) == 0)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(lines[2], "BINARY", 6) == 0)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(lines[3], "DATASET STRUCTURED_POINTS", 25) == 0)) {
        std::fclose(fp);
        return false;
    }
    uint32_t dx, dy, dz = 0;
    if (!(std::strncmp(lines[4], "DIMENSIONS", 10) == 0 &&
          std::sscanf(lines[4], "%*s %u %u %u", &dx, &dy, &dz) == 3)) {
        std::fclose(fp);
        return false;
    }
    float sx, sy, sz = 0.0f;
    // spacing and origin might be swapped, so we need to check both lines 5 and 6
    if (!((std::strncmp(lines[5], "SPACING", 7) == 0 &&
           std::sscanf(lines[5], "%*s %f %f %f", &sx, &sy, &sz) == 3) ||
          (std::strncmp(lines[6], "SPACING", 7) == 0 &&
           std::sscanf(lines[6], "%*s %f %f %f", &sx, &sy, &sz) == 3))) {
        std::fclose(fp);
        return false;
    }
    size_t num_voxels = 0;
    if (!(std::strncmp(lines[7], "POINT_DATA", 10) == 0 &&
          std::sscanf(lines[7], "%*s %lu", &num_voxels) == 1)) {
        std::fclose(fp);
        return false;
    }
    assert(size_t(dx) * dy * dz == num_voxels);
    char data_type[256];
    if (!(std::strncmp(lines[8], "SCALARS", 7) == 0 &&
          std::sscanf(lines[8], "%*s %*s %s", data_type) == 1)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(data_type, "unsigned_char", 13) == 0)) {
        std::fclose(fp);
        return false;
    }

    volume.dimensions = glm::uvec3(dx, dy, dz);
    volume.spacing = glm::vec3(sx, sy, sz);

    // Read data
    volume.data.resize(num_voxels);
    const size_t result = std::fread(volume.data.data(), sizeof(uint8_t), num_voxels, fp);
    std::fclose(fp);
    if (result != num_voxels) {
        return false;
    }

    return true;
}

void create_max_blocks(const VolumeUInt8 &volume, const glm::uvec3 &num_blocks,
                       VolumeUInt8 &max_blocks)
{
    assert(volume.dimensions[0] > 0);
    assert(volume.dimensions[1] > 0);
    assert(volume.dimensions[2] > 0);
    assert(num_blocks[0] > 0);
    assert(num_blocks[1] > 0);
    assert(num_blocks[2] > 0);

    const size_t width = volume.dimensions[0];
    const size_t height = volume.dimensions[1];
    const size_t depth = volume.dimensions[2];
    assert(width * height * depth == volume.data.size());

    const double block_width = double(width) / num_blocks[0];
    const double block_height = double(height) / num_blocks[1];
    const double block_depth = double(depth) / num_blocks[2];

    max_blocks.dimensions = num_blocks;
    max_blocks.spacing = glm::vec3(block_width, block_height, block_depth) * volume.spacing;
    max_blocks.data.resize(num_blocks[0] * num_blocks[1] * num_blocks[2]);

    #pragma omp parallel for schedule(dynamic, 1)
    for (size_t k = 0; k < num_blocks[2]; ++k) {
        for (size_t j = 0; j < num_blocks[1]; ++j) {
            for (size_t i = 0; i < num_blocks[0]; ++i) {
                // Determine block bounds
                const size_t ii_min = clamp(size_t(std::floor(i * block_width)), 0, width - 1);
                const size_t ii_max = clamp(size_t(std::ceil((i + 1) * block_width)), 0, width);
                const size_t jj_min = clamp(size_t(std::floor(j * block_height)), 0, height - 1);
                const size_t jj_max = clamp(size_t(std::ceil((j + 1) * block_height)), 0, height);
                const size_t kk_min = clamp(size_t(std::floor(k * block_depth)), 0, depth - 1);
                const size_t kk_max = clamp(size_t(std::ceil((k + 1) * block_depth)), 0, depth);

                // Find maximum intensity value in block
                uint8_t max_intensity = 0;
                for (size_t kk = kk_min; kk < kk_max; ++kk) {
                    for (size_t jj = jj_min; jj < jj_max; ++jj) {
                        for (size_t ii = ii_min; ii < ii_max; ++ii) {
                            const size_t voxel_index = ii + jj * width + kk * width * height;
                            const uint8_t intensity = volume.data[voxel_index];
                            max_intensity = intensity > max_intensity ? intensity : max_intensity;
                        }
                    }
                }

                const size_t block_index =
                    i + j * num_blocks[0] + k * num_blocks[0] * num_blocks[1];
                max_blocks.data[block_index] = max_intensity;
            }
        }
    }
}

} // namespace isopt
