#!/bin/bash

export ISOPATH_ROOT=$(pwd)

# Generate build script
cd $ISOPATH_ROOT && \
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=$ISOPATH_ROOT ../ && \

# Build and install the program
make -j4 && \
make install && \

# Run the program
cd ../bin && \
./isopath
