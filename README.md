# IsoPath
IsoPath is a GPU-accelerated path tracer with progressive refinement that can be used for generating isosurface visualizations of volumetric images or voxelized meshes. It uses traditional ray marching combined with empty-space skipping techniques to traverse the volume and find surface intersection points for primary and secondary rays. The renderer is implemented in C++ using OpenGL 4.5 and GLSL compute shaders and supports the following features:

- GPU-accelerated ray marching with progressive refinement
- Dynamic isovalues (can be changed with a slider)
- Empty-space skipping
- Low-discrepancy sampling with digit-permuted Halton sequences. A per-pixel shift (Cranley-Patterson rotation with blue noise) is applied on the sequences to randomize them between pixels.
- Blue noise jittering of ray start positions. Hides artifacts from undersampling.
- Interval refinement of isosurface intersections. Improves the quality of gradients/normals.
- Matte, glossy, metal, and attenuated glass materials
- Sampling of equirectangular HDR environment maps (.hdr images)
- A simple denoiser based on bilateral filtering and temporal anti-aliasing (TAA). Takes color, normals, and depth into account.
- Filmic tone mapping and bloom

Empty-space skipping is performed by traversing a max intensity block volume with analytic ray-box intersection tests. The ray marching is then started at the first block that has an intensity value larger than the isovalue. This reduces the number of costly 3D texture samples and speeds up the ray marching substantially.

## Example screenshot
![Screenshot](https://bitbucket.org/johannysjo/ptvol_isosurf/raw/master/resources/screenshot_isopath_dragons.png "Screenshot")

Voxelized Stanford Dragon model, rendered with a glossy material and three glass materials with different attenuation coefficients.

## Compiling (Linux)
Clone the git repository and run (from the root folder)
```bash
$ ./build.sh
```
Requires CMake 3.0 or higher and a C++11 capable compiler (GCC 4.6+).

## Usage
Set the environment variable
```bash
$ export ISOPATH_ROOT=/path/to/isopath/directory
```
and run the program as
```bash
$ bin/isopath data/dragon_256_uint8.vtk
```
The renderer can read volume image files in the legacy VTK uint8 format. It also supports loading of HDR environment maps (32-bit .hdr images), which can be downloaded from [here](https://polyhaven.com/hdris). Volumes and HDR environment maps can be loaded into the program with drag and drop.

## License
IsoPath is provided under the MIT license. See LICENSE.txt for more information.

## More screenshots
Isosurface visualizations of the MANIX CT volume dataset from the OsiriX DICOM image library:

![Manix isosurface](https://bitbucket.org/johannysjo/ptvol_isosurf/raw/master/resources/screenshot_isopath_manix.png)

Voxelized XYZ RGB dragon model, rendered as an isosurface with matte, glossy, metal, and glass materials:

![XYZRGB dragon isosurface](https://bitbucket.org/johannysjo/ptvol_isosurf/raw/master/resources/screenshot_isopath_xyzrgb_dragons.png)

Visualization of the [stag beetle](https://www.cg.tuwien.ac.at/research/publications/2005/dataset-stagbeetle/) CT volume dataset from TU Wien. The right image shows the max intensity block volume structure used for empty-space skipping.

![Empty-space skipping](https://bitbucket.org/johannysjo/ptvol_isosurf/raw/master/resources/screenshot_isopath_empty_space_skipping.png)

ImGui user interface:

![UI](https://bitbucket.org/johannysjo/ptvol_isosurf/raw/master/resources/screenshot_isopath_ui.png)
